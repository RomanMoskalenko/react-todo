import React from 'react';
import TodoList from './todo/TodoList';

function App() {
  return (
  <div className="wrapper">
    <h1>React tutorial</h1>
    <TodoList />
  </div>);
}

export default App;
